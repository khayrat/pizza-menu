export const urlImageValidation = (message) => ({
  validator: (_, value) => urlImageValidator(value, message),
});

const urlImageValidator = (value, message) => {
  const regexImage = /(https?:\/\/.*\.(?:png|jpg))/i;
  if (!value || regexImage.test(value)) {
    return Promise.resolve();
  }
  return Promise.reject(message);
};

export const validPrice = (message) => ({
  validator: (_, value) => validPriceValidator(value, message),
});

const validPriceValidator = (value, message) => {
  if (!value || value >= 0) {
    return Promise.resolve();
  }
  return Promise.reject(message);
};
