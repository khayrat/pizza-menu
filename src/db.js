const db = [
  {
    id: "OL7353617M",
    title: "Fantastic Mr. Fox",
    year: "2019",
    author: "Roald Dahl",
    description:
      "And these two very old people are the father and mother of Mrs. Bucket.",
  },
];

export default db;
