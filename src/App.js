import React from "react";
import { Route } from "react-router-dom";
import Home from "./component/Home/Home";
import EditCreateItem from "./component/EditCreateItem/EditCreateItem";

import "./App.css";
import "react-toastify/dist/ReactToastify.css";

class App extends React.Component {
  componentDidMount() {}

  render() {
    return (
      <div className="app">
        <Route exact path={["/", "/search"]} render={() => <Home />} />
        <Route exact path="/create" render={() => <EditCreateItem />} />
        <Route exact path="/edit/:id" render={() => <EditCreateItem />} />
      </div>
    );
  }
}

export default App;
