import { uid } from "../utils/helper";

let db = [
  {
    id: "_1yezku28o",
    title: "Piazza1",
    price: 10,
    type: "main",
    description:
      "And these two very old people are the father and mother of Mrs. Bucket.",
    img: "http://placehold.jp/500x500.png",
  },
  {
    id: uid(),
    title: "Piazza2",
    price: 10,
    type: "main",
    description:
      "And these two very old people are the father and mother of Mrs. Bucket.",
    img: "http://placehold.jp/500x500.png",
  },
  {
    id: uid(),
    title: "Piazza3",
    price: 10,
    type: "main",
    description:
      "And these two very old people are the father and mother of Mrs. Bucket.",
    img: "http://placehold.jp/500x500.png",
  },
  {
    id: uid(),
    title: "Piazza4",
    price: 10,
    type: "main",
    description:
      "And these two very old people are the father and mother of Mrs. Bucket.",
    img: "http://placehold.jp/500x500.png",
  },
  {
    id: uid(),
    title: "Piazza5",
    price: 10,
    type: "main",
    description:
      "And these two very old people are the father and mother of Mrs. Bucket.",
    img: "http://placehold.jp/500x500.png",
  },
  {
    id: uid(),
    title: "Piazza6",
    price: 10,
    type: "main",
    description:
      "And these two very old people are the father and mother of Mrs. Bucket.",
    img: "http://placehold.jp/500x500.png",
  },
  {
    id: uid(),
    title: "Piazza7",
    price: 10,
    type: "main",
    description:
      "And these two very old people are the father and mother of Mrs. Bucket.",
    img: "http://placehold.jp/500x500.png",
  },
];

export default db;
