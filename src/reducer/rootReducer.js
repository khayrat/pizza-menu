import { combineReducers } from "redux";
import PizzaItems from "./PizzaItems";

export default combineReducers({
  PizzaItems,
});
