import actionTypes from "../action/ActionTypes";
import db from "./db";
import { uid } from "../utils/helper";

const initialState = {
  PizzaItems: db,
  EditItem: null,
};

export default function(state = initialState, action) {
  const returnIdItems = (id) => {
    return state.PizzaItems.findIndex((item) => item.id === id);
  };

  switch (action.type) {
    case actionTypes.ADD_PIZZA:
      return {
        ...state,
        PizzaItems: [...state.PizzaItems, ...[{ id: uid(), ...action.data }]],
      };

    case actionTypes.DELETE_PIZZA:
      const indexArrayDelete = returnIdItems(action.id);

      state.PizzaItems.splice(indexArrayDelete, 1);
      return { ...state, PizzaItems: [...state.PizzaItems] };

    case actionTypes.GET_PIZZA_ITEM:
      const indexArray = returnIdItems(action.id);
      return { ...state, EditItem: { ...state.PizzaItems[indexArray] } };

    case actionTypes.EDIT_PIZZA_MENUS_REQUEST:
      const indexArrayEdit = returnIdItems(action.data.id);
      state.PizzaItems[indexArrayEdit] = action.data;
      return {
        ...state,
      };
    default:
      return state;
  }
}
