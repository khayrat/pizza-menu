import actionTypes from "./ActionTypes";

export const getPizzaItems = () => ({
  type: actionTypes.GET_PIZZA_MENUS,
});

export const deletePizzaItemAction = (id) => ({
  type: actionTypes.DELETE_PIZZA,
  id,
});

export const addPizzaAction = (data) => ({
  type: actionTypes.ADD_PIZZA,
  data,
});

export const editPizzaAction = (data) => ({
  type: actionTypes.EDIT_PIZZA_MENUS_REQUEST,
  data,
});

export const getPizzaItem = (id) => ({
  type: actionTypes.GET_PIZZA_ITEM,
  id,
});
