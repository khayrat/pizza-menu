import React from "react";
import { withRouter } from "react-router";
import { Button, PageHeader } from "antd";

import "./Header.css";

const Header = (props) => {
  const handleOnClick = () => {
    const { history } = props;
    history.push("/create");
  };

  return (
    <>
      <PageHeader
        className="br-site-header"
        title="Pizza"
        extra={[
          <Button key="3" onClick={() => handleOnClick()}>
            Add New Pizza
          </Button>,
        ]}
      />
    </>
  );
};

export default withRouter(Header);
