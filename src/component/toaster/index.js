import { toast } from "react-toastify";
import { SUCCESS } from "../../constant/Toast";

import "react-toastify/dist/ReactToastify.css";

const toastConfigObject = {
  position: "top-center",
  autoClose: 5000,
  hideProgressBar: true,
  closeOnClick: true,
  pauseOnHover: false,
  draggable: true,
};

toast.configure(toastConfigObject);

const notify = (msg, type) =>
  type === SUCCESS ? toast.success(msg) : toast.error(msg);

export default notify;
