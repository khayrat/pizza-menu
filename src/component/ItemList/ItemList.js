import React from "react";
import { withRouter } from "react-router";
import { Card } from "antd";
import { EditOutlined, DeleteOutlined } from "@ant-design/icons";
import { connect } from "react-redux";

import { deletePizzaItemAction } from "../../action/index";

import notify from "../toaster/index";

import "./ItemList.css";

const ItemList = ({ item, deletePizzaItem, history }) => {
  const handleOnDeleted = (id) => {
    try {
      deletePizzaItem(id);
      notify("this item is deleted sucessfully", "success");
    } catch (err) {
      notify(err.message);
    }
  };

  const handleOnEdit = (id) => {
    history.push(`/edit/${id}`);
  };

  return (
    <>
      <Card
        hoverable
        cover={<img alt={item.title} src={item.img} />}
        actions={[
          <DeleteOutlined
            key="delete"
            onClick={() => handleOnDeleted(item.id)}
          />,
          <EditOutlined key="edit" onClick={() => handleOnEdit(item.id)} />,
        ]}
      >
        <Card.Meta
          title={
            <>
              <h4>
                {item.title} - {item.type}
              </h4>
              <p>{item.price}</p>
            </>
          }
          description={item.description.substring(0, 24) + "..."}
        />
      </Card>
    </>
  );
};

const mapDispatchToProps = (dispatch) => ({
  deletePizzaItem: (id) => dispatch(deletePizzaItemAction(id)),
});

export default connect(null, mapDispatchToProps)(withRouter(ItemList));
