import React, { useRef, useEffect } from "react";
import { withRouter } from "react-router";
import { connect } from "react-redux";
import { Input, InputNumber, Form, Button, Select } from "antd";

import {
  addPizzaAction,
  editPizzaAction,
  getPizzaItem,
} from "../../action/index";
import { urlImageValidation, validPrice } from "../../utils/form";

import notify from "../toaster/index";

import "./EditCreateItem.css";

const EditCreateItem = (props) => {
  const formRef = useRef();
  const {
    match: {
      params: { id },
    },
    history,
    EditItem,
    addPizza,
    editPizza,
    getPizzaSingleItem,
  } = props;

  useEffect(() => {
    if (id) {
      getPizzaSingleItem(id);
    }
  }, []);

  useEffect(() => {
    if (EditItem && id) {
      const { description, img, title, price, type } = EditItem;
      formRef.current.setFieldsValue({ description, img, title, price, type });
    }
  }, [EditItem, id]);

  const handleOnSubmit = (values) => {
    try {
      if (id) {
        editPizza({ ...values, id });
        notify("update done", "success");
        history.push("/");
      } else {
        addPizza(values);
        notify("added done", "success");
        history.push("/");
      }
    } catch (err) {
      notify(err);
    }
  };

  const handleOnClickCancel = () => {
    history.push("/");
  };

  return (
    <div className="br-create-edit">
      <h1>{id ? "edit pizza" : "create new piazza"}</h1>
      <Form
        name="control-ref"
        ref={formRef}
        onFinish={(values) => handleOnSubmit(values)}
      >
        <Form.Item
          name="title"
          label="title "
          rules={[{ required: true, message: "this field is required" }]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          name="price"
          label="Price "
          rules={[
            { required: true, message: "this field is required" },
            validPrice("price can't be minus"),
          ]}
        >
          <InputNumber />
        </Form.Item>
        <Form.Item
          name="img"
          label="Image"
          rules={[urlImageValidation("this isn't valid image url")]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          name="type"
          label="Type "
          rules={[{ required: true, message: "this field is required" }]}
        >
          <Select>
            <Select.Option value="main">main</Select.Option>
            <Select.Option value="side">side</Select.Option>
          </Select>
        </Form.Item>
        <Form.Item
          name="description"
          label="Description "
          rules={[{ required: true, message: "this field is required" }]}
        >
          <Input.TextArea autoSize={{ minRows: 3.1, maxRows: 3.1 }} />
        </Form.Item>

        <Form.Item className="br-create-edit__buttons">
          <Button className="mr-10" type="primary" htmlType="submit">
            save
          </Button>
          <Button onClick={() => handleOnClickCancel()}>cancel</Button>
        </Form.Item>
      </Form>
    </div>
  );
};

const mapDispatchToProps = (dispatch) => ({
  addPizza: (data) => dispatch(addPizzaAction(data)),
  editPizza: (data) => dispatch(editPizzaAction(data)),
  getPizzaSingleItem: (id) => dispatch(getPizzaItem(id)),
});

const mapStateToProps = ({ PizzaItems }) => {
  return { ...PizzaItems };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(EditCreateItem));
