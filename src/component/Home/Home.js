import React, { useState, useEffect, useRef } from "react";
import { withRouter } from "react-router";
import { connect } from "react-redux";
import { Input, Form, Button, Select, List } from "antd";

import { deletePizzaItemAction } from "../../action/index";

import Header from "../Header/Header";
import ItemList from "../ItemList/ItemList";

import "./Home.css";

const Home = (props) => {
  const {
    PizzaItems,
    location: { search },
  } = props;
  const [listPizza, setListPizza] = useState([]);
  const formRef = useRef();
  const [searchValue, setSearchValue] = useState({});

  useEffect(() => {
    var params = new URLSearchParams(search);
    params.forEach(function(value, key) {
      formRef.current.setFieldsValue({ [key]: value });
      setSearchValue((prev) => ({ ...prev, [key]: value }));
    });
  }, [search]);

  useEffect(() => {
    setListPizza(PizzaItems);
  }, [PizzaItems]);

  useEffect(() => {
    handleSearch(searchValue.title, searchValue.type);
  }, [searchValue]);

  const handleSearch = (title, type = null) => {
    if (title && type) {
      setListPizza(
        PizzaItems.filter(
          (item) => item.title.includes(title) && type === item.type
        )
      );
    } else if (title) {
      setListPizza(PizzaItems.filter((item) => item.title.includes(title)));
    } else if (type) {
      setListPizza(PizzaItems.filter((item) => type === item.type));
    } else {
      setListPizza(PizzaItems);
    }
  };

  const handleOnFinish = (values) => {
    const { title, type } = values;
    handleSearch(title, type);
  };

  return (
    <>
      <Header />
      <div className="br-home">
        <h1>Pizza Menu</h1>
        <Form
          name="basic"
          className="br-home__search"
          ref={formRef}
          onFinish={handleOnFinish}
        >
          <div className="br-home__search__row">
            <Form.Item name="title" label={"Name"}>
              <Input type="text" />
            </Form.Item>

            <Form.Item name="type" label={"Course Type"}>
              <Select allowClear>
                <Select.Option value="main">main</Select.Option>
                <Select.Option value="side">side</Select.Option>
              </Select>
            </Form.Item>
            <Form.Item className="br-create-edit__buttons">
              <Button className="mr-10" type="primary" htmlType="submit">
                save
              </Button>
            </Form.Item>
          </div>
        </Form>

        <List
          className="br-home-container"
          dataSource={listPizza}
          grid={{
            xs: 1,
            sm: 2,
            md: 3,
            lg: 3,
            xl: 3,
            xxl: 3,
          }}
          pagination={{
            pageSize: 6,
          }}
          renderItem={(item) => (
            <List.Item>
              <ItemList item={item} />
            </List.Item>
          )}
        />
      </div>
    </>
  );
};

const mapDispatchToProps = (dispatch) => ({
  deletePizzaItem: (id) => dispatch(deletePizzaItemAction(id)),
});

const mapStateToProps = ({ PizzaItems }) => {
  return { ...PizzaItems };
};

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Home));
